-module(server).
% -behaviour(gen_server).

% application behaviour
-export([start/2, stop/1]).


% gen_server api
-export([start_link/1]).

% gen_server callbacks
% -export([init/1, handle_call/3, handle_cast/2]).


%% Starts the Yggdrasil network. Opens a listen port at the port defined and opens Acceptors number of concurrent acceptors.
start(normal, _Args) ->
    {ok, ListenSocket} = gen_tcp:listen(12345, [{active,once}, {reuseaddr, true},inet6]),
    {ok, Sup} = server_sup:start_link(ListenSocket),
    _ = [ supervisor:start_child(Sup, []) || _ <- lists:seq(1,Acceptors)],
    {ok, Sup, ListenSocket}.

%% Stop the Yggdrasil app (closes the listen socket)
stop(ListenSocket) ->
    ok = gen_tcp:close(ListenSocket),
    ok.

start_link(Socket) ->
    gen_server:start_link(?MODULE, Socket, []).






